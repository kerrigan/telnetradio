# -*- coding: utf-8 -*-
import os
import random

from twisted.internet import reactor
from twisted.internet.protocol import Factory
from twisted.protocols.basic import LineReceiver

import midi

SPEED_CONST = 200.

class Radio(LineReceiver):

    def connectionMade(self):
        cached = self.factory.lyric_buffer
        if cached:
            self.transport.write(cached)
        self.factory.register(self)

    def connectionLost(self, reason):
        self.factory.unregister(self)


class RadioFactory(Factory):
    protocol = Radio
    lyric_buffer = ""
    clients = []

    def __init__(self, path):
        self.path = path
        self.send_lyrics(self.next_file())
    
    def register(self, client):
        self.clients.append(client)

    def unregister(self, client):
        self.clients.remove(client)

    def broadcast(self, message):
        if message.startswith("\\") or message.startswith("/"):
            message = message[1:]
            message = "\r\n" + message
        self.lyric_buffer += message
        for client in self.clients:
            client.transport.write(message)

    def send_lyrics(self, filename):
        print "NP: " + filename
        self.lyric_buffer = ""
        midifile = midi.read_midifile(filename)
        lyrics = [i for i in midifile if any(map(lambda t: isinstance(t, midi.TextMetaEvent), i))]
        lyrics = max(lyrics)
        timer = 3
        for chunk in lyrics:
            text = "".join(map(chr, chunk.data)).decode('cp1251').encode('utf-8')
            reactor.callLater(timer, self.broadcast, text)
            sleep =(chunk.tick / SPEED_CONST)
            if sleep > 2:
                sleep = 1
            timer += sleep
        reactor.callLater(timer, self.send_lyrics, self.next_file())

    def next_file(self):
        files = filter(lambda f: f.endswith(".kar"), os.listdir(self.path))
        return os.path.join(self.path, random.choice(files))



if __name__ == '__main__':
    reactor.listenTCP(9876, RadioFactory("songs"))
    reactor.run()
